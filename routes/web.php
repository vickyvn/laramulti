<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\ChangePasswordController;
use App\Http\Controllers\Auth\VerificationApiController;
use Illuminate\Foundation\Auth\EmailVerificationRequest;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);

Route::get('/adminHome', [App\Http\Controllers\HomeController::class, 'adminHome'])->name('adminHome');
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/vendor/home', [HomeController::class, 'vendorHome'])->name('vendor.home')->middleware('is_vendor');
Route::get('/vendor/register', [RegisterController::class, 'vendorRegister'])->name('vendor.register');
Route::delete('/adminHome/customer/delete/{id}', [HomeController::class, 'deleteCustomer'])->name('delete.customer');
Route::delete('/adminHome/vendor/delete/{id}', [HomeController::class, 'deleteVendor'])->name('delete.vendor');
Route::get('/edit/{id}/user', [HomeController::class, 'editUser'])->name('edit.user');
Route::post('update/user/{id}', [HomeController::class, 'updateUser'])->name('update.user');

Route::get('/email/verify', function () {
    return view('auth.verify-email');
})->middleware('auth')->name('verification.notice');

Route::get('/email/verify/{id}/{hash}', function (EmailVerificationRequest $request) {
    $request->fulfill();

    return redirect('/home');
})->middleware(['auth', 'signed'])->name('verification.verify');

Route::post('/email/verification-notification', function (Request $request) {
    $request->user()->sendEmailVerificationNotification();

    return back()->with('message', 'Verification link sent!');
})->middleware(['auth', 'throttle:6,1'])->name('verification.send');

Route::get('/profile', function () {
    // Only verified users may access this route...
})->middleware('verified');

Route::get('change-password', [ChangePasswordController::class, 'index'])->name('password');
Route::post('change-password', [ChangePasswordController::class, 'store'])->name('change.password');

Route::get('/login/{social}', [LoginController::class, 'socialLogin'])->where('social','facebook|google');
Route::get('/login/{social}/callback', [LoginController::class, 'handleProviderCallback'])->where('social','facebook|google');

