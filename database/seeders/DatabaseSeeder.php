<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'first_name' => 'vicky',
            'last_name' => 'nehare',
            'is_admin' => 1,
            'email' =>'vickynehare.vn@mail.com',
            'password' => bcrypt('123456')
        ]);
    }
}
