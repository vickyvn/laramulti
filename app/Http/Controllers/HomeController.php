<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Vendor;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('verified');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Show the application admin dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function adminHome()
    {

        $customerUser = User::where('is_customer', 1)->where('is_admin', 0)->get();
        $vendorUser = User::with('vendor')->where('is_customer', 0)->where('is_admin', 0)->get();
        
        return view('adminHome', ['customerUser' => $customerUser, 'vendorUser' => $vendorUser]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function vendorHome()
    {
        return view('vendorHome');
    }

    /**
     * Edit user.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function editUser(Request $request)
    {
        $user = User::with('vendor')->where('id', $request['id'])->first();
        
        return view('auth.editUser', ['user' => $user]);
    }


    /**
     * update user.
     *
     * @param  array  $request
     * @return \App\Models\User
     */
    protected function updateUser(Request $request)
    {
        $user =  User::where('id', $request['id'])->update([
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
        ]);

        if($request['is_customer'] == 0 && $request['is_admin'] == 0) {
             Vendor::where('user_id', $request['id'])->update([
                'store_name' => $request['store_name'],
                'store_address' => $request['store_address'],
                'store_description' => $request['store_description'],
                'contact_number' => $request['contact_number'],
            ]);
        }

        return redirect()->back()->with('success', 'updated user');   
    }

    /**
     * Deelete customer.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function deleteCustomer($id)
    {
        $deleteUser = User::where('id', $id)->delete();

        return redirect()->back()->with('success', 'delete customer');   

    }

    /**
     * Deelete vendor.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function deleteVendor($id)
    {
        $deleteUser = User::where('id', $id)->delete();
        $deleteVendor = Vendor::where('user_id', $id)->delete();

        return redirect()->back()->with('success', 'delete customer');   

    }
}
