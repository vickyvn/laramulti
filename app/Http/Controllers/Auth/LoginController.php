<?php

namespace App\Http\Controllers\Auth;

use Socialite;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Login user.
     *
     * @return void
     */
    public function login(Request $request)
    {   
        $input = $request->all();

        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if(auth()->attempt(array('email' => $input['email'], 'password' => $input['password']))){
            if (auth()->user()->is_customer == 0 && auth()->user()->is_admin == 0) {
                return redirect()->route('vendor.home');
            } else if(auth()->user()->is_admin == 1){
                return redirect()->route('adminHome');
            } else {
                return redirect()->route('home');
            }
        } else {
            return redirect()->route('login')->with('error','Email-Address And Password Are Wrong.');
        }
    }

    /**
    * Handle Social login request
    *
    * @return response
    */
    public function socialLogin($social)
    {
        return Socialite::driver($social)->redirect();
    }
    /**
        * Obtain the user information from Social Logged in.
        * @param $social
        * @return Response
        */
    public function handleProviderCallback($social)
    {
        $userSocial = Socialite::driver($social)->user();
        $user = User::where(['email' => $userSocial->getEmail()])->first();
        if($user){
            Auth::login($user);

            // email data
            $email_data = array(
                'first_name' => $user['first_name'],
                'last_name' => $user['last_name'],
                'email' => $user['email'],
            );


            // send email with the template
            Mail::send('welcome_email', $email_data, function ($message) use ($email_data) {
                $message->to($email_data['email'], $email_data['first_name'])
                    ->subject('Welcome to alkurn')
                    ->from('vickynehare.vn@gmail.com', 'alkurn');
            });
            return redirect()->action('HomeController@index');
        }else{
            return view('auth.register',['name' => $userSocial->getName(), 'email' => $userSocial->getEmail()]);
        }
    }


}
