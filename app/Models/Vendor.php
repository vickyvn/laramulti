<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vendor extends Model
{
    use HasFactory;

    protected $guard = 'vendor';

    protected $fillable = [
        'user_id',
        'store_name', 
        'store_address', 
        'store_description', 
        'contact_number', 
        'profile_picture', 
        'banner_picture', 
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Get the user that owns the phone.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
