@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard <a href="{{ route('password') }}" class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">Change Password</a>
                <a href="{{ route('edit.user', Auth::user()->id) }}" class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">Update User Details</a></div>
            </div>
                <div class="card-body">
                    
                    Welcome Admin
                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        Logout
                    </a>
                    
                </div>
                <h1>Customer List</h1>
                <table class="pack-table">
                    <tr>
                        <th width="30%">First Name</th>
                        <th width="30%">Last Name</th>
                        <th width="30%">Email</th>
                        <th width="30%">Action</th>
                    </tr>
                    @foreach($customerUser as $customer)
                    <tr>
                        <td  >{{ $customer->first_name; }}</td>
                        <td>{{ $customer->last_name; }}</td>
                        <td >{{ $customer->email; }}</td>
                        <td>
                            <form action="{{ route('delete.customer',$customer->id) }}" method="POST">
            
                                
            
                                @csrf
                                @method('DELETE')
                
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                            <a href="{{ route('edit.user', $customer->id) }}" class="btn btn-warning">Edit</a>
                        </td>
                    </tr>
                    @endforeach
                </table>
            <br>
                <h1>Vendor List</h1>
                <table class="pack-table">
                    <tr>
                        <th width="30%">First Name</th>
                        <th width="30%">Last Name</th>
                        <th width="30%">Email</th>
                        <th width="30%">Store</th>
                        <th width="30%">Action</th>
                    </tr>
                    @foreach($vendorUser as $vendor)
                    <tr>
                        <td  >{{ $vendor->first_name; }}</td>
                        <td>{{ $vendor->last_name; }}</td>
                        <td >{{ $vendor->email; }}</td>
                        <td >{{ $vendor->vendor->store_name; }}</td>
                        <td>
                            <form action="{{ route('delete.vendor',$vendor->id) }}" method="POST">
            
                                
            
                                @csrf
                                @method('DELETE')
                
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                            <a href="{{ route('edit.user', $vendor->id) }}" class="btn btn-warning">Edit</a>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>
@endsection