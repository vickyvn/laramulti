@extends('layouts.app')
   
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard <a href="{{ route('password') }}" class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">Change Password</a>  <a href="{{ route('edit.user', Auth::user()->id) }}" class="ml-4 text-sm text-gray-700 dark:text-gray-500 underline">Update User Details</a></div>
                <div class="card-body">
                    You are Vendor.
                    <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                        Logout
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection