<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Mailgun, Postmark, AWS and more. This file provides the de facto
    | location for this type of information, allowing packages to have
    | a conventional file to locate the various service credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
        'endpoint' => env('MAILGUN_ENDPOINT', 'api.mailgun.net'),
    ],

    'postmark' => [
        'token' => env('POSTMARK_TOKEN'),
    ],

    'ses' => [
        'key' => env('AWS_ACCESS_KEY_ID'),
        'secret' => env('AWS_SECRET_ACCESS_KEY'),
        'region' => env('AWS_DEFAULT_REGION', 'us-east-1'),
    ],
    'facebook' => [
        'client_id' => '550483232682751', //Facebook API
        'client_secret' => 'c0d591234158f03ce68be0ba628334bc', //Facebook Secret
        'redirect' => 'http://laramulti.test/login/facebook/callback',
     ],
     'google' => [
        'client_id'     => '560487367062-k75p1pcdrhi3385rp7c6c9h1cl03rdbq.apps.googleusercontent.com',
        'client_secret' => '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',
        'redirect'      => 'http://laramulti.test/login/google/callback'
    ],
];
